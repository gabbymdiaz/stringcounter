package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StringCounterTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	// Good Input
	@Test
	public void testCountChars() {
		int totalCount = StringCounter.CountChars("gabz");
		assertTrue("Invalid input", totalCount == 4);
	}
	
	// Bad Input
	@Test(expected = Exception.class)
	public void testCountCharsException() {
		int totalCount = StringCounter.CountChars(null);
		assertFalse("Invalid input", totalCount == 0);
	}
	
	// Boundary IN
	@Test
	public void testCountCharsBoundaryIn() {
		int totalCount = StringCounter.CountChars("G");
		assertTrue("Invalid input", totalCount == 1);
	}
	
	// Boundary Out
	@Test
	public void testCountCharsBoundaryOut() {
		int totalCount = StringCounter.CountChars("5");
		assertFalse("Invalid input", totalCount == 5);
	}

}
